--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: -
--

COPY category (id, name, description, is_visible, created_at, updated_at, upper_category_id) FROM stdin;
1	Crowdfunding		t	2015-07-03 22:22:58.871663+02	2015-07-03 22:22:58.871695+02	\N
2	News		t	2015-07-03 22:23:07.898399+02	2015-07-03 22:23:07.898432+02	\N
3	Social Media		t	2015-07-03 22:23:13.869141+02	2015-07-03 22:23:13.869175+02	\N
4	Video		t	2015-08-01 08:16:09.52586+02	2015-08-01 08:16:09.525891+02	\N
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('category_id_seq', 4, true);


--
-- Data for Name: plan; Type: TABLE DATA; Schema: public; Owner: -
--

COPY plan (id, name, short_name, description, icon, is_visible, created_at, updated_at, period, product_id, amount, net_amount, ttl, ttl_description) FROM stdin;
3	Unlimited	unlimited	Unlimited plan description	\N	t	2016-04-23 20:45:07.969+02	2016-04-23 20:45:10.023+02	30	com.epicglue.plan.unlimited.1	1499	1200	86400	1 month
4	Pro	pro	Pro plan description	\N	t	2016-04-23 20:48:29.236+02	2016-04-23 20:48:30.267+02	30	com.epicglue.plan.pro.1	299	230	86400	1 month
2	Free	free	Free plan description	\N	t	2016-04-07 20:21:36.186+02	2016-04-07 20:21:39.269+02	1	free	0	0	86400	24 hours
1	Beta	beta	Short beta description	\N	t	2015-11-10 15:52:49.925902+01	2015-11-10 15:52:49.925902+01	7	beta	0	0	604800	7 days
\.


--
-- Name: plan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('plan_id_seq', 1, false);


--
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: -
--

COPY service (id, name, short_name, description, is_visible, created_at, updated_at, category_id, is_locked) FROM stdin;
5	Kickstarter	kickstarter		t	2015-05-09 14:26:33.695126+02	2015-09-11 17:29:07.33308+02	1	f
3	Hacker News	hacker_news		t	2015-05-09 14:26:01.544946+02	2015-09-11 17:30:42.74427+02	2	f
2	Twitter	twitter		t	2015-05-09 14:25:47.494133+02	2015-10-26 06:52:16.342177+01	3	t
6	YouTube	youtube		f	2015-05-09 14:26:40.423148+02	2015-10-26 06:52:25.48498+01	4	f
7	Reddit	reddit		t	2015-09-15 19:50:56.435122+02	2016-05-28 06:53:49.423907+02	3	f
4	Product Hunt	product_hunt		t	2015-05-09 14:26:19.239889+02	2016-05-28 06:53:53.916509+02	2	f
1	Instagram	instagram		f	2015-05-09 13:05:47.682286+02	2016-05-29 08:11:26.431342+02	3	t
\.


--
-- Name: service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('service_id_seq', 7, true);


--
-- Data for Name: source; Type: TABLE DATA; Schema: public; Owner: -
--

COPY source (id, script_name, name, description, item_ordering_field, allow_value, is_per_user, min_refresh, is_active, created_at, updated_at, service_id, value, url, is_public, icon, is_locked, value_hint, do_auto_subscribe) FROM stdin;
7	twitter_mention	Mentions		content_created_at	f	t	900	t	2015-05-09 14:27:58.51678+02	2015-10-13 06:01:33.62513+02	2		https://api.twitter.com	t		t		f
6	twitter_retweet_of_me	Retweets of Me		content_created_at	f	t	900	t	2015-05-09 14:27:44.396437+02	2015-10-13 06:01:37.196163+02	2		https://api.twitter.com	t		t		f
5	twitter_timeline	Timeline		content_created_at	f	t	120	t	2015-05-09 14:27:22.469496+02	2015-10-13 06:01:40.23259+02	2		https://api.twitter.com	t		t		f
19	youtube_wall	Wall		content_created_at	f	t	300	t	2015-05-09 14:36:37.689246+02	2015-10-13 06:01:46.734852+02	6		https://www.googleapis.com/youtube/v3/activities?part=snippet&home=true&maxResults=50	t		f		f
24	hacker_news_post_id	Ask HN		content_created_at	f	f	300	t	2015-08-01 10:38:12.41604+02	2015-10-26 06:11:54.916769+01	3	askstories	https://hacker-news.firebaseio.com/v0/[[value]].json	t		f		f
25	hacker_news_post_id	Jobs		content_created_at	f	f	300	t	2015-08-01 10:38:33.423628+02	2015-10-26 06:11:58.917604+01	3	jobstories	https://hacker-news.firebaseio.com/v0/[[value]].json	t		f		f
23	hacker_news_post_id	Show HN	Show HN posts	content_created_at	f	f	300	t	2015-08-01 10:37:53.327751+02	2015-10-26 06:12:09.026471+01	3	showstories	https://hacker-news.firebaseio.com/v0/[[value]].json	t		f		f
20	hacker_news_post	Single Post		content_created_at	f	f	300	t	2015-05-25 21:36:55.031513+02	2015-11-05 16:38:33.511273+01	3		https://hacker-news.firebaseio.com/v0/item/[[value]].json	f		f		f
10	hacker_news_post_id	Homepage		created_at	f	f	300	t	2015-05-09 14:29:03.970398+02	2015-11-06 16:59:22.217083+01	3	topstories	https://hacker-news.firebaseio.com/v0/[[value]].json	t		f		f
50	kickstarter_search	Film & Video		content_created_at	f	f	300	t	2016-06-23 18:48:12.664+02	2016-06-23 18:48:16.962+02	5	category_id=11,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t		f		f
39	kickstarter_search	Design		content_created_at	f	f	300	t	2016-05-28 07:02:57.751421+02	2016-05-28 07:02:57.75145+02	5	category_id=design,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
30	hacker_news_post_id	Top Stories		content_created_at	f	f	300	t	2015-11-06 17:00:01.851459+01	2015-11-06 17:01:07.797693+01	3	beststories	https://hacker-news.firebaseio.com/v0/[[value]].json	t		f		f
27	kickstarter_search	Art		content_created_at	f	f	300	t	2015-09-14 11:21:48.303137+02	2016-05-28 06:59:04.558672+02	5	category_id=art,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t		f		f
26	kickstarter_search	Comics		content_created_at	f	f	300	t	2015-09-14 11:21:27.35655+02	2016-05-28 06:59:10.030632+02	5	category_id=comics,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t		f		f
28	kickstarter_search	Games		content_created_at	f	f	300	t	2015-09-14 11:22:21.281138+02	2016-05-28 06:59:16.99622+02	5	category_id=games,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t		f		f
34	hacker_news_post_id	New Stories		content_created_at	f	f	300	t	2015-11-06 17:00:01.851+01	2015-11-06 17:01:07.797+01	3	newstories	https://hacker-news.firebaseio.com/v0/[[value]].json	t		f		f
36	kickstarter_search	Theater		content_created_at	f	f	300	t	2016-05-28 07:02:13.453905+02	2016-05-28 07:02:13.453919+02	5	category_id=theater,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
37	kickstarter_search	Crafts		content_created_at	f	f	300	t	2016-05-28 07:02:33.814057+02	2016-05-28 07:02:33.814071+02	5	category_id=crafts,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
38	kickstarter_search	Dance		content_created_at	f	f	300	t	2016-05-28 07:02:48.286629+02	2016-05-28 07:02:48.286644+02	5	category_id=dance,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
41	kickstarter_search	Food		content_created_at	f	f	300	t	2016-05-28 07:03:25.672746+02	2016-05-28 07:03:25.672766+02	5	category_id=food,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
42	kickstarter_search	Journalism		content_created_at	f	f	300	t	2016-05-28 07:03:38.640825+02	2016-05-28 07:03:38.640849+02	5	category_id=journalism,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
43	kickstarter_search	Music		content_created_at	f	f	300	t	2016-05-28 07:03:55.187975+02	2016-05-28 07:03:55.187989+02	5	category_id=music,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
44	kickstarter_search	Photography		content_created_at	f	f	300	t	2016-05-28 07:04:05.846718+02	2016-05-28 07:04:05.846734+02	5	category_id=photography,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
45	kickstarter_search	Publishing		content_created_at	f	f	300	t	2016-05-28 07:04:18.009428+02	2016-05-28 07:04:18.009441+02	5	category_id=publishing,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
46	kickstarter_search	Technology		content_created_at	f	f	300	t	2016-05-28 07:04:34.619858+02	2016-05-28 07:04:34.619872+02	5	category_id=technology,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
18	kickstarter_search	Search Results	Search Kickstarter by keyword	content_created_at	t	f	300	t	2015-05-09 14:36:14.753427+02	2016-05-28 07:05:02.706076+02	5		http://www.kickstarter.com/discover/advanced?format=json	t		f		f
12	hacker_news_search	Search Results		content_created_at	t	f	600	t	2015-05-09 14:33:57.288204+02	2016-05-28 07:05:18.392713+02	3		http://hn.algolia.com/api/v1/search_by_date?tags=story&query=[[value]]	t		f		f
8	twitter_user	User Stats		content_created_at	f	t	300	t	2015-05-09 14:28:15.907756+02	2016-05-28 07:06:09.940933+02	2		https://api.twitter.com	f		t		t
2	instagram_user	User Stats		content_created_at	f	t	300	f	2015-05-09 14:08:32.884622+02	2016-05-29 08:11:07.918479+02	1		https://api.instagram.com/v1/users/self	f		t		t
47	reddit_post	Homepage		content_created_at	f	f	300	t	2016-05-28 07:10:27.251685+02	2016-05-28 07:10:27.251699+02	7	homepage_hot	https://www.reddit.com/hot.json	t	\N	f		f
1	instagram_feed	Feed	Your Instagram Wall	content_created_at	f	t	60	f	2015-05-09 13:07:16.76163+02	2016-05-29 08:11:11.405967+02	1		https://api.instagram.com/v1/users/self/feed	t		t		f
49	product_hunt_post	Latest Products		content_created_at	f	f	300	t	2016-05-28 07:41:19.338384+02	2016-05-28 07:41:19.338397+02	4		https://api.producthunt.com/v1/posts	t	\N	f		f
40	kickstarter_search	Fashion		content_created_at	f	f	300	t	2016-05-28 07:03:09.719904+02	2016-05-28 07:03:09.719917+02	5	category_id=fashion,sort=magic	http://www.kickstarter.com/discover/advanced?format=json	t	\N	f		f
48	reddit_post	My Homepage		content_created_at	f	t	300	f	2016-05-28 07:10:45.60334+02	2016-05-28 12:33:11.419997+02	7	my_homepage_hot	https://www.reddit.com/hot.json	t	\N	t		f
29	reddit_post	Subreddit		created_at	t	f	300	t	2015-09-15 19:51:55.632197+02	2016-05-28 12:35:33.570332+02	7		https://www.reddit.com/r/[[value]].json	t		f		f
\.


--
-- Name: source_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('source_id_seq', 50, true);


--
-- PostgreSQL database dump complete
--

