SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE category (
  id integer NOT NULL,
  upper_category_id integer,
  name character varying(20) NOT NULL,
  description character varying(255),
  is_visible boolean NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone
);


--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE category_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE category_id_seq OWNED BY category.id;


--
-- Name: feedback; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE feedback (
  id integer NOT NULL,
  user_id integer NOT NULL,
  feedback text NOT NULL,
  created_at timestamp with time zone NOT NULL
);


--
-- Name: feedback_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE feedback_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: feedback_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE feedback_id_seq OWNED BY feedback.id;


--
-- Name: item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE item (
  hash character varying(64) NOT NULL,
  content_id character varying(100) NOT NULL,
  content_secondary_id character varying(100),
  content_created_at timestamp with time zone NOT NULL,
  content_updated_at timestamp with time zone,
  item_type character varying(30) NOT NULL,
  media_type character varying(30) NOT NULL,
  service character varying(30) NOT NULL,
  title text,
  description text,
  media jsonb[],
  author text NOT NULL,
  author_media jsonb,
  tags text[] DEFAULT ARRAY[]::character varying[] NOT NULL,
  location jsonb,
  url text,
  url_ext text,
  url_in text,
  visibility character varying(10) NOT NULL,
  points integer,
  comments integer,
  is_deleted boolean DEFAULT false,
  content_order_by_date timestamp with time zone NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone,
  deleted_at timestamp with time zone
);


--
-- Name: item_raw; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE item_raw (
  hash character varying(64) NOT NULL,
  json jsonb,
  text text,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone
);


--
-- Name: plan; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE plan (
  id integer NOT NULL,
  name character varying(100) NOT NULL,
  description character varying(255),
  product_id character varying(30) NOT NULL,
  amount integer NOT NULL,
  net_amount integer NOT NULL,
  ttl integer NOT NULL,
  ttl_description character varying(255) NOT NULL,
  is_visible boolean NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone
);


--
-- Name: plan_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE plan_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: plan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE plan_id_seq OWNED BY plan.id;


--
-- Name: service; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE service (
  id integer NOT NULL,
  category_id integer NOT NULL,
  name character varying(100) NOT NULL,
  short_name character varying(30) NOT NULL,
  description character varying(255),
  is_locked boolean DEFAULT false,
  is_visible boolean DEFAULT false,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone
);


--
-- Name: service_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE service_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE service_id_seq OWNED BY service.id;


--
-- Name: channel; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE channel (
  id integer NOT NULL,
  user_id integer,
  service_profile_id integer,
  cmd character varying(255) NOT NULL,
  name character varying(30) NOT NULL,
  description character varying(100),
  tag character varying(30) NOT NULL,
  is_active boolean NOT NULL,
  min_refresh integer NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone
);


--
-- Name: channel_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE channel_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: channel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE channel_id_seq OWNED BY channel.id;


--
-- Name: pipe; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE pipe (
  id integer NOT NULL,
  user_id integer NOT NULL,
  channels integer[] NOT NULL,
  query jsonb,
  tag character varying(100) NOT NULL,
  is_active boolean NOT NULL,
  is_deleted boolean NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone
);


--
-- Name: pipe_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pipe_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: pipe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pipe_id_seq OWNED BY pipe.id;


--
-- Name: user_service_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_service_token (
  id integer NOT NULL,
  token character varying(255) NOT NULL,
  token_secret character varying(255),
  refresh_token character varying(255),
  expiry timestamp with time zone,
  is_active boolean NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone,
  profile_id integer,
  scope character varying(255)
);


--
-- Name: token_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE token_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE token_id_seq OWNED BY user_service_token.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "user" (
  id integer NOT NULL,
  username character varying(100) NOT NULL,
  device_udid_list character varying(64)[],
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone,
  password character varying(255),
  salt character varying(255)
);


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: user_item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_item (
  id integer NOT NULL,
  user_id integer NOT NULL,
  item_id character varying(64) NOT NULL,
  channels integer[] NOT NULL,
  is_read boolean DEFAULT false,
  is_glued boolean DEFAULT false,
  is_deleted boolean DEFAULT false,
  is_indexed boolean DEFAULT false,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone
);


--
-- Name: user_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_item_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: user_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_item_id_seq OWNED BY user_item.id;


--
-- Name: user_metric; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_metric (
  id integer NOT NULL,
  key character varying(200) NOT NULL,
  value numeric(16,4) NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone,
  user_id integer
);


--
-- Name: user_metric_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_metric_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: user_metric_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_metric_id_seq OWNED BY user_metric.id;


--
-- Name: user_payment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_payment (
  id integer NOT NULL,
  amount integer NOT NULL,
  net_amount integer NOT NULL,
  created_at timestamp with time zone NOT NULL,
  user_id integer NOT NULL,
  product_id character varying(255) NOT NULL,
  payment_id character varying(255) NOT NULL,
  original_payment_id character varying(255) NOT NULL,
  payment_time timestamp with time zone NOT NULL,
  quantity integer DEFAULT 1
);


--
-- Name: user_payment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_payment_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: user_payment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_payment_id_seq OWNED BY user_payment.id;


--
-- Name: user_plan; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_plan (
  id integer NOT NULL,
  user_id integer NOT NULL,
  plan_id integer NOT NULL,
  ttl integer NOT NULL,
  valid_from date NOT NULL,
  valid_until date NOT NULL,
  is_active boolean DEFAULT true,
  created_at timestamp with time zone DEFAULT now()
);


--
-- Name: user_plan_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_plan_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: user_plan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_plan_id_seq OWNED BY user_plan.id;


--
-- Name: user_service_profile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_service_profile (
  id integer NOT NULL,
  identifier character varying(50) NOT NULL,
  friendly_name character varying(50) NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone,
  service_id integer NOT NULL,
  user_id integer
);


--
-- Name: user_service_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_service_profile_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: user_service_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_service_profile_id_seq OWNED BY user_service_profile.id;


--
-- Name: user_social; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_social (
  id integer NOT NULL,
  json text NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone,
  service_id integer NOT NULL,
  user_id integer NOT NULL
);


--
-- Name: user_social_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_social_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: user_social_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_social_id_seq OWNED BY user_social.id;


--
-- Name: user_stat; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_stat (
  id integer NOT NULL,
  date date NOT NULL,
  key character varying(30) NOT NULL,
  value integer NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone,
  profile_id integer NOT NULL,
  user_id integer
);


--
-- Name: user_stat_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_stat_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: user_stat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_stat_id_seq OWNED BY user_stat.id;


--
-- Name: user_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_token (
  id integer NOT NULL,
  token character varying(255) NOT NULL,
  expiry timestamp with time zone,
  is_active boolean NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone,
  user_id integer
);


--
-- Name: user_token_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_token_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: user_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_token_id_seq OWNED BY user_token.id;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY category ALTER COLUMN id SET DEFAULT nextval('category_id_seq'::regclass);

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY feedback ALTER COLUMN id SET DEFAULT nextval('feedback_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY plan ALTER COLUMN id SET DEFAULT nextval('plan_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY service ALTER COLUMN id SET DEFAULT nextval('service_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY channel ALTER COLUMN id SET DEFAULT nextval('channel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pipe ALTER COLUMN id SET DEFAULT nextval('pipe_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_item ALTER COLUMN id SET DEFAULT nextval('user_item_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_metric ALTER COLUMN id SET DEFAULT nextval('user_metric_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_payment ALTER COLUMN id SET DEFAULT nextval('user_payment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_plan ALTER COLUMN id SET DEFAULT nextval('user_plan_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_service_profile ALTER COLUMN id SET DEFAULT nextval('user_service_profile_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_service_token ALTER COLUMN id SET DEFAULT nextval('token_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_social ALTER COLUMN id SET DEFAULT nextval('user_social_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_stat ALTER COLUMN id SET DEFAULT nextval('user_stat_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_token ALTER COLUMN id SET DEFAULT nextval('user_token_id_seq'::regclass);

--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category
  ADD CONSTRAINT category_pkey PRIMARY KEY (id);

--
-- Name: feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY feedback
  ADD CONSTRAINT feedback_pkey PRIMARY KEY (id);


--
-- Name: item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY item
  ADD CONSTRAINT item_pkey PRIMARY KEY (hash);


--
-- Name: item_raw_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY item_raw
  ADD CONSTRAINT item_raw_pkey PRIMARY KEY (hash);


--
-- Name: plan_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY plan
  ADD CONSTRAINT plan_pkey PRIMARY KEY (id);


--
-- Name: plan_product_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY plan
  ADD CONSTRAINT plan_product_id_key UNIQUE (product_id);


--
-- Name: service_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY service
  ADD CONSTRAINT service_pkey PRIMARY KEY (id);


--
-- Name: service_short_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY service
  ADD CONSTRAINT service_short_name_key UNIQUE (short_name);


--
-- Name: channel_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY channel
  ADD CONSTRAINT channel_pkey PRIMARY KEY (id);


--
-- Name: pipe_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pipe
  ADD CONSTRAINT pipe_pkey PRIMARY KEY (id);


--
-- Name: token_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_service_token
  ADD CONSTRAINT token_pkey PRIMARY KEY (id);


--
-- Name: user_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_item
  ADD CONSTRAINT user_item_pkey PRIMARY KEY (id);


--
-- Name: user_item_user_id_item_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_item
  ADD CONSTRAINT user_item_user_id_item_id_key UNIQUE (user_id, item_id);


--
-- Name: user_metric_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_metric
  ADD CONSTRAINT user_metric_pkey PRIMARY KEY (id);


--
-- Name: user_metric_user_id_7dd117b840e2cff5_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_metric
  ADD CONSTRAINT user_metric_user_id_7dd117b840e2cff5_uniq UNIQUE (user_id, key);


--
-- Name: user_payment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_payment
  ADD CONSTRAINT user_payment_pkey PRIMARY KEY (id);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "user"
  ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_plan_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_plan
  ADD CONSTRAINT user_plan_pkey PRIMARY KEY (id);


--
-- Name: user_service_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_service_profile
  ADD CONSTRAINT user_service_profile_pkey PRIMARY KEY (id);


--
-- Name: user_service_profile_user_id_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_service_profile
  ADD CONSTRAINT user_service_profile_user_id_uniq UNIQUE (user_id, service_id, identifier);


--
-- Name: user_social_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_social
  ADD CONSTRAINT user_social_pkey PRIMARY KEY (id);


--
-- Name: user_stat_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_stat
  ADD CONSTRAINT user_stat_pkey PRIMARY KEY (id);


--
-- Name: user_token_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_token
  ADD CONSTRAINT user_token_pkey PRIMARY KEY (id);


--
-- Name: category_fb3a7ebf; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX category_fb3a7ebf ON category USING btree (upper_category_id);

--
-- Name: feedback_e8701ad4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX feedback_e8701ad4 ON feedback USING btree (user_id);


--
-- Name: item_hash_6ac4b279b3a9e07e_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX item_hash_6ac4b279b3a9e07e_like ON item USING btree (hash varchar_pattern_ops);


--
-- Name: item_raw_hash_7d1c02afc03e11d5_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX item_raw_hash_7d1c02afc03e11d5_like ON item_raw USING btree (hash varchar_pattern_ops);


--
-- Name: service_b583a629; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX service_b583a629 ON service USING btree (category_id);


--
-- Name: service_short_name_1723a1b2fbd6ece7_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX service_short_name_1723a1b2fbd6ece7_like ON service USING btree (short_name varchar_pattern_ops);


--
-- Name: pipe_e8701ad4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pipe_e8701ad4 ON pipe USING btree (user_id);


--
-- Name: token_83a0eb3f; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX token_83a0eb3f ON user_service_token USING btree (profile_id);


--
-- Name: user_item_82bfda79; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_item_82bfda79 ON user_item USING btree (item_id);


--
-- Name: user_item_e8701ad4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_item_e8701ad4 ON user_item USING btree (user_id);


--
-- Name: user_item_item_id_699036a6b3177965_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_item_item_id_699036a6b3177965_like ON user_item USING btree (item_id varchar_pattern_ops);


--
-- Name: user_metric_e8701ad4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_metric_e8701ad4 ON user_metric USING btree (user_id);


--
-- Name: user_payment_e8701ad4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_payment_e8701ad4 ON user_payment USING btree (user_id);


--
-- Name: user_payment_payment_id_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX user_payment_payment_id_uindex ON user_payment USING btree (payment_id);


--
-- Name: user_service_profile_b0dc1e29; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_service_profile_b0dc1e29 ON user_service_profile USING btree (service_id);


--
-- Name: user_service_profile_e8701ad4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_service_profile_e8701ad4 ON user_service_profile USING btree (user_id);


--
-- Name: user_social_b0dc1e29; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_social_b0dc1e29 ON user_social USING btree (service_id);


--
-- Name: user_social_e8701ad4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_social_e8701ad4 ON user_social USING btree (user_id);


--
-- Name: user_stat_83a0eb3f; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_stat_83a0eb3f ON user_stat USING btree (profile_id);


--
-- Name: user_stat_e8701ad4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_stat_e8701ad4 ON user_stat USING btree (user_id);


--
-- Name: user_token_e8701ad4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_token_e8701ad4 ON user_token USING btree (user_id);

--
-- Name: category_upper_category_id_fk_category_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category
  ADD CONSTRAINT category_upper_category_id_fk_category_id FOREIGN KEY (upper_category_id) REFERENCES category(id) DEFERRABLE INITIALLY DEFERRED;

--
-- Name: feedback_user_id_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY feedback
  ADD CONSTRAINT feedback_user_id_fk_user_id FOREIGN KEY (user_id) REFERENCES "user" DEFERRABLE INITIALLY DEFERRED;


--
-- Name: service_category_id_fk_category_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY service
  ADD CONSTRAINT service_category_id_fk_category_id FOREIGN KEY (category_id) REFERENCES category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pipe_user_id_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pipe
  ADD CONSTRAINT pipe_user_id_fk_user_id FOREIGN KEY (user_id) REFERENCES "user" DEFERRABLE INITIALLY DEFERRED;


--
-- Name: token_profile_id_fk_user_service_profile_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_service_token
  ADD CONSTRAINT token_profile_id_fk_user_service_profile_id FOREIGN KEY (profile_id) REFERENCES user_service_profile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_item_item_id_fk_item_hash; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_item
  ADD CONSTRAINT user_item_item_id_fk_item_hash FOREIGN KEY (item_id) REFERENCES item(hash) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_item_user_id_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_item
  ADD CONSTRAINT user_item_user_id_fk_user_id FOREIGN KEY (user_id) REFERENCES "user" DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_payment_user_id_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_payment
  ADD CONSTRAINT user_payment_user_id_fk_user_id FOREIGN KEY (user_id) REFERENCES "user" DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_service_profile_service_id_fk_service_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_service_profile
  ADD CONSTRAINT user_service_profile_service_id_fk_service_id FOREIGN KEY (service_id) REFERENCES service(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_service_profile_user_id_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_service_profile
  ADD CONSTRAINT user_service_profile_user_id_fk_user_id FOREIGN KEY (user_id) REFERENCES "user" DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_social_service_id_fk_service_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_social
  ADD CONSTRAINT user_social_service_id_fk_service_id FOREIGN KEY (service_id) REFERENCES service(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_social_user_id_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_social
  ADD CONSTRAINT user_social_user_id_fk_user_id FOREIGN KEY (user_id) REFERENCES "user" DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_sta_profile_id_fk_user_service_profile_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_stat
  ADD CONSTRAINT user_sta_profile_id_fk_user_service_profile_id FOREIGN KEY (profile_id) REFERENCES user_service_profile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_stat_user_id_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_stat
  ADD CONSTRAINT user_stat_user_id_fk_user_id FOREIGN KEY (user_id) REFERENCES "user" DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_item_user_id_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_item
  ADD CONSTRAINT user_item_user_id_fk_user_id FOREIGN KEY (user_id) REFERENCES "user" DEFERRABLE INITIALLY DEFERRED;

--
-- PostgreSQL database dump complete
--