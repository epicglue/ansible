Changelog
=========

v0.1.0
------

*Unreleased*

- Add Changelog. [drybjed]

- Fix deprecation warnings in Ansible 2.1.0. [ypid]

- Default to ``enabled: True`` in ``rsyslog_pools``.
  Before this, an entry missing a ``enabled`` has been ignored. [ypid]

