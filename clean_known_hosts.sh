#!/usr/bin/env bash
cat /dev/null > "/home/marek/.ssh/known_hosts"

ssh-keyscan lb1 lb2 es1 es2 es3 db1 db2 cache1 cache2 web1 web2 logs1 ci1 app1 app2 >> ~/.ssh/known_hosts