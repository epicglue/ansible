# This guide is optimized for Vagrant 1.7 and above.
# Although versions 1.6.x should behave very similarly, it is recommended
# to upgrade instead of disabling the requirement below.
Vagrant.require_version ">= 1.7.0"

Vagrant.configure(2) do |config|

	config.ssh.forward_agent = true

	# Load balancer
	(1..2).each do |i|
		config.vm.define "lb#{i}" do |node|
			node.vm.box = "ubuntu/trusty64"
			node.vm.hostname = "lb#{i}"
			node.vm.network "private_network", ip: "10.0.15.1#{i}"
			node.vm.network "forwarded_port", guest: 80, host: "808#{i-1}"
			node.vm.provider "virtualbox" do |vb|
				vb.memory = "256"
			end
		end
	end

	# ES
	(1..3).each do |i|
		config.vm.define "es#{i}" do |node|
			node.vm.box = "ubuntu/trusty64"
			node.vm.hostname = "es#{i}"
			node.vm.network "private_network", ip: "10.0.15.2#{i}"
			node.vm.network "forwarded_port", guest: 9200, host: "920#{i}"
			node.vm.provider "virtualbox" do |vb|
				vb.memory = "4096"
			end
		end
	end

	# DB
	(1..2).each do |i|
		config.vm.define "db#{i}" do |node|
			node.vm.box = "ubuntu/trusty64"
			node.vm.hostname = "db#{i}"
			node.vm.network "private_network", ip: "10.0.15.3#{i}"
			node.vm.provider "virtualbox" do |vb|
				vb.memory = "1024"
			end
		end
	end

	# Cache
	(1..2).each do |i|
		config.vm.define "cache#{i}" do |node|
			node.vm.box = "ubuntu/trusty64"
			node.vm.hostname = "cache#{i}"
			node.vm.network "private_network", ip: "10.0.15.4#{i}"
			node.vm.provider "virtualbox" do |vb|
				vb.memory = "1024"
			end
		end
	end

	# Web
	(1..2).each do |i|
		config.vm.define "web#{i}" do |node|
			node.vm.box = "ubuntu/trusty64"
			node.vm.hostname = "web#{i}"
			node.vm.network "private_network", ip: "10.0.15.5#{i}"
			node.vm.network "forwarded_port", guest: 80, host: "810#{i}"
			node.vm.provider "virtualbox" do |vb|
				vb.memory = "1024"
			end
		end
	end

	# Logs
	(1..1).each do |i|
		config.vm.define "logs#{i}" do |node|
			node.vm.box = "ubuntu/trusty64"
			node.vm.hostname = "logs#{i}"
			node.vm.network "private_network", ip: "10.0.15.6#{i}"
			node.vm.network "forwarded_port", guest: 5601, host: "560#{i}"
			node.vm.network "forwarded_port", guest: 9200, host: "921#{i}"
			node.vm.provider "virtualbox" do |vb|
				vb.memory = "1024"
			end
		end
	end

	# CI
	(1..1).each do |i|
		config.vm.define "ci#{i}" do |node|
			node.vm.box = "ubuntu/trusty64"
			node.vm.hostname = "ci#{i}"
			node.vm.network "private_network", ip: "10.0.15.7#{i}"
			node.vm.provider "virtualbox" do |vb|
				vb.memory = "1024"
			end
		end
  end

  # App
  (1..2).each do |i|
    config.vm.define "app#{i}" do |node|
      node.vm.box = "ubuntu/trusty64"
      node.vm.hostname = "app#{i}"
      node.vm.network "private_network", ip: "10.0.15.8#{i}"
      node.vm.network "forwarded_port", guest: 7000, host: "700#{i}"
      node.vm.network "forwarded_port", guest: 8000, host: "800#{i}"
      node.vm.provider "virtualbox" do |vb|
        vb.memory = "1024"
      end
    end
  end
end
