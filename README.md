# Before running Ansible on fresh server:
- Install Python
- Configure hostname
- Restart machine

## Dev
```ansible-playbook playbook.yml -i inventories/dev -u vagrant```

## Test

First time:

```ansible-playbook playbook.yml -i inventories/test --ask-pass -u root```

Further runs:

```ansible-playbook playbook.yml -i inventories/test --ask-sudo-pass -u marek```
